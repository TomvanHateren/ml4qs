import copy

import pandas as pd

from PythonCode.Chapter3.KalmanFilters import KalmanFilters
from PythonCode.util.VisualizeDataset import VisualizeDataset

dataset = pd.read_csv('PythonCode/intermediate_datafiles/chapter2_result.csv', index_col=0)
dataset.index = pd.to_datetime(dataset.index)

DataViz = VisualizeDataset()
filter = KalmanFilters()

kalman_data = filter.apply_kalman_filter(copy.deepcopy(dataset), 'hr_watch_rate')
DataViz.plot_imputed_values(dataset, ['original', 'kalman'], 'hr_watch_rate', kalman_data['hr_watch_rate_kalman'])
DataViz.plot_dataset(kalman_data, ['hr_watch_rate', 'hr_watch_rate_kalman'], ['exact','exact'], ['line', 'line'])