import pandas as pd

from PythonCode.util.VisualizeDataset import VisualizeDataset
from PythonCode.Chapter3.OutlierDetection import DistributionBasedOutlierDetection
from PythonCode.Chapter3.OutlierDetection import DistanceBasedOutlierDetection
import copy

dataset_path = './PythonCode/intermediate_datafiles/'
dataset = pd.read_csv(dataset_path + 'chapter2_result.csv', index_col=0)
dataset.index = pd.to_datetime(dataset.index)

OutlierDistr = DistributionBasedOutlierDetection()
OutlierDist = DistanceBasedOutlierDetection()
DataViz = VisualizeDataset()

outlier_column = 'acc_phone_x'

#print "Chauvenet"
#dataset = OutlierDistr.chauvenet(dataset, outlier_column, 0.001)
#DataViz.plot_binary_outliers(dataset, outlier_column, outlier_column + '_outlier')
#dataset = OutlierDistr.chauvenet(dataset, outlier_column, 2)
#DataViz.plot_binary_outliers(dataset, outlier_column, outlier_column + '_outlier')
#dataset = OutlierDistr.chauvenet(dataset, outlier_column, 10)
#DataViz.plot_binary_outliers(dataset, outlier_column, outlier_column + '_outlier')

#print "Mixture Model"
#dataset1 = OutlierDistr.mixture_model(dataset, outlier_column, 1, 1)
#DataViz.plot_dataset(dataset1, [outlier_column, outlier_column + '_mixture'], ['exact','exact'], ['line', 'points'])

#dataset2 = OutlierDistr.mixture_model(dataset, outlier_column, 3, 1)
#DataViz.plot_dataset(dataset2, [outlier_column, outlier_column + '_mixture'], ['exact','exact'], ['line', 'points'])

#dataset3 = OutlierDistr.mixture_model(dataset, outlier_column, 3, 3)
#DataViz.plot_dataset(dataset3, [outlier_column, outlier_column + '_mixture'], ['exact','exact'], ['line', 'points'])

#print "Simple Distance-Based Approach"
#dataset = OutlierDist.simple_distance_based(dataset, [outlier_column], 'euclidean', 0.10, 0.99)
#DataViz.plot_binary_outliers(dataset, outlier_column, 'simple_dist_outlier')

#dataset = OutlierDist.simple_distance_based(dataset, [outlier_column], 'euclidean', 0.05, 0.99)
#DataViz.plot_binary_outliers(dataset, outlier_column, 'simple_dist_outlier')

#dataset = OutlierDist.simple_distance_based(dataset, [outlier_column], 'euclidean', 0.20, 0.99)
#DataViz.plot_binary_outliers(dataset, outlier_column, 'simple_dist_outlier')

#dataset = OutlierDist.simple_distance_based(dataset, [outlier_column], 'euclidean', 0.10, 0.50)
#DataViz.plot_binary_outliers(dataset, outlier_column, 'simple_dist_outlier')

print "Local outlier factor"
dataset1 = OutlierDist.local_outlier_factor(dataset, [outlier_column], 'euclidean', 5)
DataViz.plot_dataset(dataset1, [outlier_column, 'lof'], ['exact','exact'], ['line', 'points'])

dataset2 = OutlierDist.local_outlier_factor(dataset, [outlier_column], 'euclidean', 3)
DataViz.plot_dataset(dataset2, [outlier_column, 'lof'], ['exact','exact'], ['line', 'points'])

dataset3 = OutlierDist.local_outlier_factor(dataset, [outlier_column], 'euclidean', 10)
DataViz.plot_dataset(dataset3, [outlier_column, 'lof'], ['exact','exact'], ['line', 'points'])
