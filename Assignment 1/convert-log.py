import os
import pandas as pd

log_file = open("/home/tom/OneDrive/Documents/Vrije Universiteit/jaar 4/Machine Learning for the Quantified Self/log.txt", "r")

dataset_path = './datasets/homemade/csv-participant-one/'
if not os.path.exists(dataset_path):
	os.makedirs(dataset_path)

lines_processed = 0

line = log_file.readline().strip()
lines_processed += 1
assert line == "statusId | label"

label_map = {}

while True:
	parts = log_file.readline().strip().split("|")
	lines_processed += 1
	try:
		num = int(parts[0])
		label_map[num] = parts[1]
	except ValueError:
		break

label_file = open(os.path.join(dataset_path, "labels.csv"), "w")
label_file.write("sensor_type,device_type,label,label_start,label_start_datetime,label_end,label_end_datetime\n")

light_file = open(os.path.join(dataset_path, "light_phone.csv"), "w")
light_file.write("sensor_type,device_type,timestamps,lux\n")

accel_file = open(os.path.join(dataset_path, "accelerometer_phone.csv"), "w")
accel_file.write("sensor_type,device_type,timestamps,x,y,z\n")

gyros_file = open(os.path.join(dataset_path, "gyroscope_phone.csv"), "w")
gyros_file.write("sensor_type,device_type,timestamps,x,y,z\n")

magnt_file = open(os.path.join(dataset_path, "magnetometer_phone.csv"), "w")
magnt_file.write("sensor_type,device_type,timestamps,x,y,z\n")

proxi_file = open(os.path.join(dataset_path, "proximity_phone.csv"), "w")
proxi_file.write("sensor_type,device_type,timestamps,distance\n")

start_timestamp = None
last_timestamp = None
last_label = None

while True:
	parts = log_file.readline().strip().split("|")
	if not parts:
		break

	lines_processed += 1

	try:
		num = int(parts[0])
		timestamp = parts[3] + "000000"

		if start_timestamp is None:
			start_timestamp = timestamp
			last_timestamp = timestamp
			last_label = label_map.get(num)
		else:
			last_timestamp = timestamp

		new_label = label_map.get(num)
		if last_label != new_label:
			label_file.write("interval_label,smartphone,{0},{1}.000,{2},{3}.000,{4}\n".format(
				last_label,
				start_timestamp,
				pd.to_datetime(float(start_timestamp)),
				last_timestamp,
				pd.to_datetime(float(last_timestamp)))
			)
			start_timestamp = timestamp
			last_label = new_label

		value = parts[2]
		(x, y, z) = value[1:len(value)-1].split(",")
		x = float(x)
		y = float(y)
		z = float(z)

		sensor = parts[1]
		if sensor == "light-bh1745":
			light_file.write("light,smartphone,{0},{1:.3f}\n".format(timestamp, x))
		elif sensor == "accelerometer-icm20690":
			accel_file.write("accelerometer,smartphone,{0},{1:.3f},{2:.3f},{3:.3f}\n".format(timestamp, x, y, z))
		elif sensor == "gyroscope-icm20690":
			gyros_file.write("gyroscope,smartphone,{0},{1:.3f},{2:.3f},{3:.3f}\n".format(timestamp, x, y, z))
		elif sensor == "mag-akm09911":
			magnt_file.write("magnetometer,smartphone,{0},{1:.3f},{2:.3f},{3:.3f}\n".format(timestamp, x, y, z))
		elif sensor == "proximity-pa224":
			proxi_file.write("proximity,smartphone,{0},{1:.3f}\n".format(timestamp, x))
		else:
			print "Error sensor was: " + sensor
			exit(-1)

	except ValueError:
		break

label_file.write("interval_label,smartphone,{0},{1}.000,{2},{3}.000,{4}\n".format(
	last_label,
	start_timestamp,
	pd.to_datetime(float(start_timestamp)),
	last_timestamp,
	pd.to_datetime(float(last_timestamp)))
)

print "{0} lines processed".format(lines_processed)
