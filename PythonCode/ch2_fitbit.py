##############################################################
#                                                            #
#    Mark Hoogendoorn and Burkhardt Funk (2017)              #
#    Machine Learning for the Quantified Self                #
#    Springer                                                #
#    Chapter 2                                               #
#                                                            #
##############################################################


dataset_path = '../datasets/fitbit/'
result_dataset_path = './intermediate_datafiles/fitbit/'

# Import the relevant classes.

from Chapter2.CreateDataset import CreateDataset
from util.VisualizeDataset import VisualizeDataset
from util import util
import copy
import os


if not os.path.exists(result_dataset_path):
	print('Creating result directory: ' + result_dataset_path)
	os.makedirs(result_dataset_path)

# Chapter 2: Initial exploration of the dataset.

# Set a granularity (i.e. how big are our discrete time steps). We start very
# coarse grained, namely one measurement per minute, and secondly use four measurements
# per second

granularities = [86400000 * 7, 86400000]
datasets = []

for milliseconds_per_instance in granularities:
	DataSet = CreateDataset(dataset_path, milliseconds_per_instance)
	DataSet.add_numerical_dataset('OneYearFitBitData.csv', 'Date', ['CaloriesBurned', 'Steps', 'Floors', 'MinutesOfSedentaryActivity', 'MinutesOfLightActivity', 'MinutesOfModerateActivity', 'MinutesOfIntenseActivity', 'CaloriesBurnedDuringActivity'], 'avg', '')
	DataSet.add_numerical_dataset('OneYearFitBitDataSleep.csv', 'Date', ['MinutesOfSleep', 'MinutesOfBeingAwake', 'NumberOfAwakings', 'LengthOfRestInMinutes'], 'avg', '')
	dataset = DataSet.data_table

	DataViz = VisualizeDataset()
	DataViz.plot_dataset(dataset, ['CaloriesBurned', 'Steps', 'Floors', 'Minutes', 'CaloriesBurnedDuringActivity', 'NumberOfAwakings', 'LengthOfRestInMinutes'], ['like', 'like', 'like', 'like', 'like', 'like', 'like'], ['line', 'line', 'line', 'line', 'line', 'line', 'line'])

	util.print_statistics(dataset)

	datasets.append(copy.deepcopy(dataset))
util.print_latex_table_statistics_two_datasets(datasets[0], datasets[1])

# Finally, store the last dataset we have generated (250 ms).
dataset.to_csv(result_dataset_path + 'chapter2_result.csv')
